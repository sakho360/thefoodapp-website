import 'package:flutter/cupertino.dart';
import 'package:the_food_app/utils/globals.dart';

/// Created by Musa Usman on 26.10.2020
/// Copyright © 2020 Musa Usman. All rights reserved.

class DesignGuidelines {
  ///General margin from left side for the whole website.
  double get left => sizeConfig.width(0.06);

  ///General margin from right side for the whole website.
  double get right => sizeConfig.width(0.06);

  EdgeInsets get horizontal => EdgeInsets.only(
    left: left,
    right: right,
  );

  EdgeInsets get vertical => EdgeInsets.only(
    top: 0,
    bottom: 0,
  );
}
