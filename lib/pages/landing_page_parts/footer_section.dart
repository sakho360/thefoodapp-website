part of 'package:the_food_app/pages/landing_page.dart';

/// Created by Musa Usman on 27.10.2020
/// Copyright © 2020 Musa Usman. All rights reserved.

class _FooterSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) => ScreenTypeLayout(
        desktop: _FooterSectionContentDesktop(),
        mobile: _FooterSectionContentMobile(),
      );
}

class _FooterSectionContentDesktop extends StatefulWidget {
  @override
  __FooterSectionContentDesktopState createState() =>
      __FooterSectionContentDesktopState();
}

class __FooterSectionContentDesktopState
    extends State<_FooterSectionContentDesktop> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: sizeConfig.height(1),
      color: Colors.black,
      padding: EdgeInsets.only(
        top: sizeConfig.height(.05),
        right: guidelines.right,
        left: guidelines.left,
      ),
      child: Stack(
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Positioned(
            right: sizeConfig.width(.05),
            child: Container(
              margin: EdgeInsets.only(
                top: sizeConfig.height(.075),
              ),
              // child: Image.network(
              //   "https://musausman.com/wp-content/uploads/2020/11/footer-main.png",
              //   height: sizeConfig.height(1),
              //   fit: BoxFit.fitHeight,
              // ),
              child: CachedNetworkImage(
                imageUrl: "https://musausman.com/wp-content/uploads/2020/11/footer-main.png",
                progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                  height: sizeConfig.height(0.04),
                  width: sizeConfig.height(0.04),
                  child: FittedBox(
                    alignment: Alignment.center,
                    fit: BoxFit.contain,
                    child: CircularProgressIndicator(
                      value: downloadProgress.progress
                    ),
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
                height: sizeConfig.height(1),
                fit: BoxFit.fitHeight,
                alignment: Alignment.bottomCenter,
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///Spacing
              SizedBox(
                height: sizeConfig.height(.05),
              ),

              ///App Logo
              AppLogo(),

              ///Spacing
              SizedBox(
                height: sizeConfig.height(.03),
              ),

              ///Heading
              Text(
                "Get a mobile app for your restaurant!",
                style: Theme.of(context).textTheme.headline2.copyWith(
                      color: Colors.white,
                    ),
              ),

              ///Spacing
              SizedBox(
                height: sizeConfig.height(.02),
              ),

              ///Heading
              Text(
                "Contact Us Right Now!",
                style: Theme.of(context).textTheme.headline2.copyWith(
                      color: AppTheme.primaryColor,
                      fontSize: 28,
                    ),
              ),

              ///WhatsApp Number
              Container(
                margin: EdgeInsets.only(
                  top: sizeConfig.height(.05),
                ),
                child: Row(
                  children: [
                    Icon(
                      LineAwesomeIcons.what_s_app,
                      color: Colors.grey,
                      size: 40,
                    ),
                    SizedBox(
                      width: sizeConfig.width(.005),
                    ),
                    Text(
                      "+92 324 9066001",
                      style: Theme.of(context).textTheme.headline2.copyWith(
                            color: Colors.grey,
                            fontSize: 28,
                          ),
                    ),
                  ],
                ),
              ),

              ///Website
              Container(
                margin: EdgeInsets.only(
                  top: sizeConfig.height(.015),
                ),
                child: Row(
                  children: [
                    Icon(
                      LineAwesomeIcons.globe,
                      color: Colors.grey,
                      size: 40,
                    ),
                    SizedBox(
                      width: sizeConfig.width(.005),
                    ),
                    Text(
                      "musausman.com",
                      style: Theme.of(context).textTheme.headline2.copyWith(
                            color: Colors.grey,
                            fontSize: 28,
                          ),
                    ),
                  ],
                ),
              ),

              ///Spacing
              SizedBox(
                height: sizeConfig.height(.05),
              ),

              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ///Try a live demo
                    Text(
                      "Download the live demo",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: Colors.grey),
                    ),

                    ///Icon
                    Container(
                      margin: EdgeInsets.only(
                        top: sizeConfig.height(.005),
                        left: sizeConfig.width(.005),
                      ),
                      child: Transform.rotate(
                        angle: pi,
                        child: Icon(
                          Icons.keyboard_backspace_sharp,
                          size: sizeConfig.height(.03),
                          color: Colors.grey,
                        ),
                      ),
                    ),

                    ///Get It On Google Play
                    HoverScaleButton(
                      onTap: () {
                        js.context.callMethod('open', [
                          'https://play.google.com/store/apps/details?id=com.eatsadeal.merchants'
                        ]);
                      },
                      // child: Image.network(
                      //   'https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png',
                      //   height: sizeConfig.height(.16),
                      //   width: sizeConfig.height(.4),
                      // ),
                      child: CachedNetworkImage(
                        imageUrl: "https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png",
                        progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                          height: sizeConfig.height(0.04),
                          width: sizeConfig.height(0.04),
                          child: FittedBox(
                            alignment: Alignment.center,
                            fit: BoxFit.contain,
                            child: CircularProgressIndicator(
                              value: downloadProgress.progress
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        height: sizeConfig.height(.18),
                        fit: BoxFit.fitHeight,
                        alignment: Alignment.bottomCenter,
                      ),
                    ),
                  ],
                ),
              ),

              Expanded(
                child: Row(
                  children: [
                    Text(
                      "Contact us & get your app",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: Colors.grey),
                    ),

                    ///Icon
                    Container(
                      margin: EdgeInsets.only(
                        top: sizeConfig.height(.005),
                        left: sizeConfig.width(.005),
                      ),
                      child: Transform.rotate(
                        angle: pi,
                        child: Icon(
                          Icons.keyboard_backspace_sharp,
                          size: sizeConfig.height(.03),
                          color: Colors.grey,
                        ),
                      ),
                    ),

                    ///Spacing
                    SizedBox(
                      width: sizeConfig.width(.01),
                    ),

                    ///WhatsApp Contact Button
                    HoverScaleButton(
                      onTap: () {
                        js.context.callMethod('alert', [
                          'Our Official WhatsApp:\n+923249066001\n\nContact us right now to get a stunning new mobile app for your restaurant!'
                        ]);
                      },
                      // child: Image.network(
                      //   "https://musausman.com/wp-content/uploads/2020/11/whatsapp-icon.png",
                      //   height: sizeConfig.height(.1),
                      //   width: sizeConfig.height(.1),
                      // ),
                      child: CachedNetworkImage(
                        imageUrl: "https://musausman.com/wp-content/uploads/2020/11/whatsapp-icon.png",
                        progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                          height: sizeConfig.height(0.04),
                          width: sizeConfig.height(0.04),
                          child: FittedBox(
                            alignment: Alignment.center,
                            fit: BoxFit.contain,
                            child: CircularProgressIndicator(
                              value: downloadProgress.progress
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        height: sizeConfig.height(.1),
                        width: sizeConfig.height(.1),
                        fit: BoxFit.contain,
                        alignment: Alignment.bottomCenter,
                      ),
                    ),

                    ///Mail Contact Button
                    HoverScaleButton(
                      onTap: () {
                        js.context
                            .callMethod('open', ['mailto:hello@musausman.com']);
                      },
                      // child: Image.network(
                      //   'https://techcommunity.microsoft.com/t5/image/serverpage/image-id/172206i70472167E79B9D0F?v=1.0',
                      //   height: sizeConfig.height(.1),
                      //   width: sizeConfig.height(.1),
                      // ),
                      child: CachedNetworkImage(
                        imageUrl: "https://techcommunity.microsoft.com/t5/image/serverpage/image-id/172206i70472167E79B9D0F?v=1.0",
                        progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                          height: sizeConfig.height(0.04),
                          width: sizeConfig.height(0.04),
                          child: FittedBox(
                            alignment: Alignment.center,
                            fit: BoxFit.contain,
                            child: CircularProgressIndicator(
                              value: downloadProgress.progress
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        height: sizeConfig.height(.1),
                        width: sizeConfig.height(.1),
                        fit: BoxFit.contain,
                        alignment: Alignment.bottomCenter,
                      ),
                    ),

                    ///Instagram Contact Button
                    HoverScaleButton(
                      onTap: () {
                        js.context.callMethod('open',
                            ['https://instagram.com/musa.usman.official']);
                      },
                      // child: Image.network(
                      //   'https://i.pinimg.com/originals/a2/5f/4f/a25f4f58938bbe61357ebca42d23866f.png',
                      //   height: sizeConfig.height(.11),
                      // ),
                      child: CachedNetworkImage(
                        imageUrl: "https://i.pinimg.com/originals/a2/5f/4f/a25f4f58938bbe61357ebca42d23866f.png",
                        progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                          height: sizeConfig.height(0.04),
                          width: sizeConfig.height(0.04),
                          child: FittedBox(
                            alignment: Alignment.center,
                            fit: BoxFit.contain,
                            child: CircularProgressIndicator(
                              value: downloadProgress.progress
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                        height: sizeConfig.height(.11),
                        fit: BoxFit.contain,
                        alignment: Alignment.bottomCenter,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _FooterSectionContentMobile extends StatefulWidget {
  @override
  __FooterSectionContentMobileState createState() =>
      __FooterSectionContentMobileState();
}

class __FooterSectionContentMobileState
    extends State<_FooterSectionContentMobile> {

  final String textMessage= 'Hi, I contacted you through your website.\nI need a Retaurant App for my Business. I am looking forward to have a talk with you.';

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      padding: EdgeInsets.only(
        top: sizeConfig.height(.05),
        right: guidelines.right,
        left: guidelines.left,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ///App Image
          // Image.network(
          //   "https://musausman.com/wp-content/uploads/2020/11/footer-main.png",
          //   width: sizeConfig.width(1),
          //   fit: BoxFit.fitWidth,
          // ),

          CachedNetworkImage(
            imageUrl: "https://musausman.com/wp-content/uploads/2020/11/footer-main.png",
            progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
              height: sizeConfig.height(0.04),
              width: sizeConfig.height(0.04),
              child: FittedBox(
                alignment: Alignment.center,
                fit: BoxFit.contain,
                child: CircularProgressIndicator(
                  value: downloadProgress.progress
                ),
              ),
            ),
            errorWidget: (context, url, error) => Icon(Icons.error),
            width: sizeConfig.width(1),
            fit: BoxFit.fitWidth,
            alignment: Alignment.bottomCenter,
          ),
          

          ///Gap
          SizedBox(
            height: sizeConfig.height(.025),
          ),

          ///App Logo
          AppLogo(mobileFullLogo: true),

          ///Gap
          SizedBox(
            height: sizeConfig.height(.025),
          ),

          ///Heading
          Text(
            "Get a mobile app for your restaurant!",
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline2.copyWith(
              color: Colors.white,
            ),
          ),

          ///Spacing
          SizedBox(
            height: sizeConfig.height(.08),
          ),

          ///Heading
          Text(
            "Contact Us Right Now!",
            style: Theme.of(context).textTheme.headline2.copyWith(
                  color: AppTheme.primaryColor,
                  fontSize: 30,
                ),
          ),

          ///WhatsApp Number
          Container(
            margin: EdgeInsets.only(
              top: sizeConfig.height(.03),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  LineAwesomeIcons.what_s_app,
                  color: Colors.grey,
                  size: 35,
                ),
                SizedBox(
                  width: sizeConfig.width(.005),
                ),
                Text(
                  "+92 324 9066001",
                  style: Theme.of(context).textTheme.headline2.copyWith(
                        color: Colors.grey,
                        fontSize: 26,
                      ),
                ),
              ],
            ),
          ),

          ///Website
          Container(
            margin: EdgeInsets.only(
              top: sizeConfig.height(.015),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  LineAwesomeIcons.globe,
                  color: Colors.grey,
                  size: 35,
                ),
                SizedBox(
                  width: sizeConfig.width(.005),
                ),
                Text(
                  "musausman.com",
                  style: Theme.of(context).textTheme.headline2.copyWith(
                        color: Colors.grey,
                        fontSize: 26,
                      ),
                ),
              ],
            ),
          ),

          ///Spacing
          SizedBox(
            height: sizeConfig.height(.07),
          ),

          ///Try a live demo
          Text(
            "Download the live demo!",
            style: Theme.of(context)
                .textTheme
                .headline5
                .copyWith(color: Colors.grey),
          ),

          ///Get It On Google Play
          HoverScaleButton(
            onTap: () {
              js.context.callMethod('open', [
                'https://play.google.com/store/apps/details?id=com.eatsadeal.merchants'
              ]);
            },
            // child: Image.network(
            //   'https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png',
            //   height: sizeConfig.height(.2),
            //   fit: BoxFit.fitHeight,
            // ),
            child: CachedNetworkImage(
              imageUrl: "https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png",
              progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                height: sizeConfig.height(0.04),
                width: sizeConfig.height(0.04),
                child: FittedBox(
                  alignment: Alignment.center,
                  fit: BoxFit.contain,
                  child: CircularProgressIndicator(
                    value: downloadProgress.progress
                  ),
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
              height: sizeConfig.height(.2),
              fit: BoxFit.fitHeight,
              alignment: Alignment.bottomCenter,
            ),
          ),

          ///Contact Us Heading
          Text(
            "Contact us & get your app!",
            style: Theme.of(context)
                .textTheme
                .headline5
                .copyWith(color: Colors.grey),
          ),

          ///Gap
          SizedBox(height: sizeConfig.height(0.04)),

          ///Contact Us Buttons
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ///WhatsApp Contact Button
              HoverScaleButton(
                onTap: () {
                  js.context.callMethod(
                    'open',
                    ['https://api.whatsapp.com/send?phone=+923249066001&text=$textMessage']);
                },
                // child: Image.network(
                //   "https://musausman.com/wp-content/uploads/2020/11/whatsapp-icon.png",
                //   height: sizeConfig.width(.25),
                //   width: sizeConfig.width(.25),
                //   fit: BoxFit.fitWidth,
                // ),
                child: CachedNetworkImage(
                  imageUrl: "https://musausman.com/wp-content/uploads/2020/11/whatsapp-icon.png",
                  progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                    height: sizeConfig.height(0.04),
                    width: sizeConfig.height(0.04),
                    child: FittedBox(
                      alignment: Alignment.center,
                      fit: BoxFit.contain,
                      child: CircularProgressIndicator(
                        value: downloadProgress.progress
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  height: sizeConfig.width(.25),
                  width: sizeConfig.width(.25),
                  fit: BoxFit.fitWidth,
                  alignment: Alignment.bottomCenter,
                ),
              ),

              ///Mail Contact Button
              HoverScaleButton(
                onTap: () {
                  js.context.callMethod('open', ['mailto:hello@musausman.com']);
                },
                // child: Image.network(
                //   'https://techcommunity.microsoft.com/t5/image/serverpage/image-id/172206i70472167E79B9D0F?v=1.0',
                //   height: sizeConfig.width(.25),
                //   width: sizeConfig.width(.25),
                //   fit: BoxFit.fitWidth,
                // ),
                child: CachedNetworkImage(
                  imageUrl: "https://techcommunity.microsoft.com/t5/image/serverpage/image-id/172206i70472167E79B9D0F?v=1.0",
                  progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                    height: sizeConfig.height(0.04),
                    width: sizeConfig.height(0.04),
                    child: FittedBox(
                      alignment: Alignment.center,
                      fit: BoxFit.contain,
                      child: CircularProgressIndicator(
                        value: downloadProgress.progress
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  height: sizeConfig.width(.25),
                  width: sizeConfig.width(.25),
                  fit: BoxFit.fitWidth,
                  alignment: Alignment.bottomCenter,
                ),
              ),

              ///Instagram Contact Button
              HoverScaleButton(
                onTap: () {
                  js.context.callMethod(
                      'open', ['https://instagram.com/musa.usman.official']);
                },
                child: Image.network(
                  'https://i.pinimg.com/originals/a2/5f/4f/a25f4f58938bbe61357ebca42d23866f.png',
                  height: sizeConfig.width(.25),
                  width: sizeConfig.width(.25),
                  fit: BoxFit.fitWidth,
                ),
              ),
            ],
          ),

          ///Gap
          SizedBox(height: sizeConfig.height(0.04)),
        ],
      ),
    );
  }
}
