/// Created by Musa Usman on 27.10.2020
/// Copyright © 2020 Musa Usman. All rights reserved.

part of 'package:the_food_app/pages/landing_page.dart';

class _FeaturesSection extends StatefulWidget {
  @override
  __FeaturesSectionState createState() => __FeaturesSectionState();
}

class __FeaturesSectionState extends State<_FeaturesSection> {
  @override
  Widget build(BuildContext context) => ScreenTypeLayout(
        desktop: _FeaturesSectionContentDesktop(),
        mobile: _FeaturesSectionContentMobile(),
      );
}

class _FeaturesSectionContentDesktop extends StatefulWidget {
  @override
  __FeaturesSectionContentDesktopState createState() =>
      __FeaturesSectionContentDesktopState();
}

class __FeaturesSectionContentDesktopState
    extends State<_FeaturesSectionContentDesktop> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: sizeConfig.height(1.4) - (kToolbarHeight * 1.5),
      color: Colors.black,
      width: double.maxFinite,
      padding: EdgeInsets.only(
        left: guidelines.left,
        right: guidelines.right,
        top: sizeConfig.height(.1),
        bottom: sizeConfig.height(.1),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Dozens of features!",
            style: Theme.of(context).textTheme.headline4.copyWith(
                  color: AppTheme.primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 35,
                ),
          ),
          SizedBox(
            height: sizeConfig.height(0.05),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: _listItemsOne(),
                  ),
                ),

                ///Spacing
                SizedBox(
                  width: sizeConfig.width(.05),
                ),

                Expanded(
                  flex: 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: _listItemsTwo(),
                  ),
                ),

                ///Spacing
                SizedBox(
                  width: sizeConfig.width(.04),
                ),

                Expanded(
                  flex: 4,
                  // child: Image.network('https://musausman.com/wp-content/uploads/2020/11/tfa-welcome-page.png'),
                  child: CachedNetworkImage(
                    imageUrl: "https://musausman.com/wp-content/uploads/2020/11/tfa-welcome-page.png",
                    progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                      height: sizeConfig.height(0.04),
                      width: sizeConfig.height(0.04),
                      child: FittedBox(
                        alignment: Alignment.center,
                        fit: BoxFit.contain,
                        child: CircularProgressIndicator(
                          value: downloadProgress.progress
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    width: sizeConfig.width(1),
                    fit: BoxFit.contain,
                    alignment: Alignment.bottomCenter,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _featureHeading(String text) => SelectableText(
        text,
        style: Theme.of(context).textTheme.headline4.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
      );

  Widget _featureDescription(String text) => Container(
        margin: EdgeInsets.only(
          top: sizeConfig.height(.0075),
          // bottom: sizeConfig.height(.02)
        ),
        child: SelectableText(
          text,
          style: Theme.of(context).textTheme.caption.copyWith(
                color: Colors.grey,
                fontSize: 17,
              ),
        ),
      );

  List<Widget> _listItemsOne() => <Widget>[
    _featureHeading("Create Account & Login"),
    _featureDescription(
        "Users can sign in or sign up using their Email Address or through Google, Facebook or Apple."),
    Spacer(),
    _featureHeading("Discounts & Deals Functionality"),
    _featureDescription(
        "You can set special discounts on items & make deals & combos."),
    Spacer(),
    _featureHeading("Add To Cart Functionality"),
    _featureDescription(
        "Users can select individual items & prepare their cart before placing an order. "),
    Spacer(),
    _featureHeading("Place Orders"),
    _featureDescription(
        "Users can place orders for their chosen items & deals."),
    Spacer(),
    _featureHeading("Ratings & Reviews Functionality"),
    _featureDescription(
        "After an order is complete, users can rate 5 stars & leave reviews."),
    Spacer(),
    _featureHeading(
        "Favorite / Save / Bookmark Functionality"),
    _featureDescription(
        "Users can save their favorite deals for quick access."),
  ];

  List<Widget> _listItemsTwo() => <Widget>[
    _featureHeading("View Restaurant Menu"),
    _featureDescription("Users can view your complete restaurant menu. Designed to be easy."),
    Spacer(),
    _featureHeading("Search Functionality"),
    _featureDescription(
        "Users can search through your menu to find what they need like a special deal or burger."),
    Spacer(),
    _featureHeading("Make Secure Online Payments"),
    _featureDescription(
        "Users can use their credit cards to make payments easily & securely."),
    Spacer(),
    _featureHeading("Live Order Tracking"),
    _featureDescription(
        "After placing an order, users can track the status of an order."),
    Spacer(),
    _featureHeading("Contact Support"),
    _featureDescription(
        "After contact your restaurant, by phone call, SMS, email & even WhatsApp!"),
  ];
}

class _FeaturesSectionContentMobile extends StatefulWidget {
  @override
  __FeaturesSectionContentMobileState createState() =>
      __FeaturesSectionContentMobileState();
}

class __FeaturesSectionContentMobileState
    extends State<_FeaturesSectionContentMobile> {
  @override
  Widget build(BuildContext context) => Container(
        height: sizeConfig.height(4) - (kToolbarHeight * 1.5),
        color: Colors.black,
        width: double.maxFinite,
        padding: EdgeInsets.only(
          left: guidelines.left,
          right: guidelines.right,
          top: sizeConfig.height(.1),
          bottom: sizeConfig.height(.1),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Dozens of features!",
              style: Theme.of(context).textTheme.headline4.copyWith(
                    color: AppTheme.primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 35,
                  ),
            ),
            SizedBox(
              height: sizeConfig.height(0.05),
            ),
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: _listItemsOne(),
                    ),
                  ),

                  ///Spacing
                  SizedBox(
                    width: sizeConfig.width(.05),
                  ),

                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: _listItemsTwo(),
                    ),
                  ),

                  ///Spacing
                  SizedBox(
                    width: sizeConfig.width(.05),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                // child: Image.network('https://musausman.com/wp-content/uploads/2020/11/tfa-welcome-page.png'),
                child: CachedNetworkImage(
                  imageUrl: "https://musausman.com/wp-content/uploads/2020/11/tfa-welcome-page.png",
                  progressIndicatorBuilder: (context, url, downloadProgress) => SizedBox(
                    height: sizeConfig.height(0.04),
                    width: sizeConfig.height(0.04),
                    child: FittedBox(
                      alignment: Alignment.center,
                      fit: BoxFit.contain,
                      child: CircularProgressIndicator(
                        value: downloadProgress.progress
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                  width: sizeConfig.width(1),
                  fit: BoxFit.contain,
                  alignment: Alignment.bottomCenter,
                ),
              ),
            ),
          ],
        ),
      );

  Widget _featureHeading(String text) => SelectableText(
        text,
        style: Theme.of(context).textTheme.headline4.copyWith(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
      );

  Widget _featureDescription(String text) => Container(
        margin: EdgeInsets.only(
          top: sizeConfig.height(.0075),
          // bottom: sizeConfig.height(.02)
        ),
        child: SelectableText(
          text,
          style: Theme.of(context).textTheme.caption.copyWith(
                color: Colors.grey,
                fontSize: 17,
              ),
        ),
      );

  List<Widget> _listItemsOne() => <Widget>[
    _featureHeading("Create Account & Login"),
    _featureDescription(
        "Users can sign in or sign up using their Email Address or through Google, Facebook or Apple."),
    Spacer(),
    _featureHeading("Discounts & Deals Functionality"),
    _featureDescription(
        "You can set special discounts on items & make deals & combos."),
    Spacer(),
    _featureHeading("Add To Cart Functionality"),
    _featureDescription(
        "Users can select individual items & prepare their cart before placing an order. "),
    Spacer(),
    _featureHeading("Place Orders"),
    _featureDescription(
        "Users can place orders for their chosen items & deals."),
    Spacer(),
    _featureHeading("Ratings & Reviews Functionality"),
    _featureDescription(
        "After an order is complete, users can rate 5 stars & leave reviews."),
    Spacer(),
    _featureHeading(
        "Favorite / Save / Bookmark Functionality"),
    _featureDescription(
        "Users can save their favorite deals for quick access."),
  ];

  List<Widget> _listItemsTwo() => <Widget>[
    _featureHeading("View Restaurant Menu"),
    _featureDescription("Users can view your complete restaurant menu. Designed to be easy."),
    Spacer(),
    _featureHeading("Search Functionality"),
    _featureDescription(
        "Users can search through your menu to find what they need like a special deal or burger."),
    Spacer(),
    _featureHeading("Make Secure Online Payments"),
    _featureDescription(
        "Users can use their credit cards to make payments easily & securely."),
    Spacer(),
    _featureHeading("Live Order Tracking"),
    _featureDescription(
        "After placing an order, users can track the status of an order."),
    Spacer(),
    _featureHeading("Contact Support"),
    _featureDescription(
        "After contact your restaurant, by phone call, SMS, email & even WhatsApp!"),
  ];
}
