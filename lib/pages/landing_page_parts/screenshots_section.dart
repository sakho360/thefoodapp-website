part of 'package:the_food_app/pages/landing_page.dart';

/// Created by Musa Usman on 11.11.2020
/// Copyright © 2020 Musa Usman. All rights reserved.
///
/// Email: hello@musausman.com
/// Website: musausman.com
/// WhatsApp: +92 324 9066001

Map<String, String> _screenshots1 = Map<String, String>.from({
  "Welcome Page":
      "https://musausman.com/wp-content/uploads/2020/11/tfa-welcome-page.png",
  "Home Page":
      "https://musausman.com/wp-content/uploads/2020/11/tfa-home-page.png",
  "Item Details":
      "https://musausman.com/wp-content/uploads/2020/11/tfa-item-detail-page.png",
  "Your Favorites":
      "https://musausman.com/wp-content/uploads/2020/11/tfa-favorites-page.png",
});

Map<String, String> _screenshots2 = Map<String, String>.from({
  "Search":
      "https://musausman.com/wp-content/uploads/2020/11/tfa-search-page.png",
  "Search Results":
      "https://musausman.com/wp-content/uploads/2020/11/tfa-search-results-page.png",
  "Your Cart":
      "https://musausman.com/wp-content/uploads/2020/11/tfa-cart-page.png",
  "Your Order History":
      "https://musausman.com/wp-content/uploads/2020/11/tfa-order-history-page.png",
});

class _ScreenshotsSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(
          vertical: sizeConfig.height(.1),
        ),
        margin: guidelines.horizontal,
        child: Column(
          children: [
            ///Gap
            SizedBox(
              height: sizeConfig.height(.04),
            ),

            ///Section Title
            Text(
              "An amazing and easy experience for your customers!",
              style: Theme.of(context).textTheme.headline4.copyWith(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 35,
                  ),
            ),

            ///Gap
            SizedBox(
              height: sizeConfig.height(.01),
            ),

            ///Section Title
            Text(
              "Here are some screenshots...",
              style: Theme.of(context).textTheme.caption.copyWith(
                    fontSize: 25,
                  ),
            ),

            ///Gap
            SizedBox(
              height: sizeConfig.height(.06),
            ),

            ///Screenshots First List
            sizeConfig.isDesktopScreen
                ? Row(children: _content(_screenshots1))
                : Column(children: _content(_screenshots1)),

            ///Screenshots Second List
            sizeConfig.isDesktopScreen
                ? Row(children: _content(_screenshots2))
                : Column(children: _content(_screenshots2)),
          ],
        ),
      );

  List<Widget> _content(Map<String, String> screenshots) => List.generate(
        screenshots.length,
        (int index) => _ScreenGridItem(
          screenshots.values.elementAt(index),
          screenshots.keys.elementAt(index),
        ),
      );
}

class _ScreenGridItem extends StatelessWidget {
  final String url;
  final String title;

  _ScreenGridItem(this.url, this.title);

  final double _screenImageWidth =
      sizeConfig.isDesktopScreen
          ? (sizeConfig.width(1) - (guidelines.left + guidelines.right)) / 4
          : sizeConfig.width(0.9);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: sizeConfig.isDesktopScreen
              ? sizeConfig.height(0.75)
              : sizeConfig.height(1),
          width: _screenImageWidth,
          child: CachedNetworkImage(
            imageUrl: url,
            progressIndicatorBuilder: (context, url, downloadProgress) => Center(
              child: Container(
                height: sizeConfig.height(.07),
                decoration: BoxDecoration(
                  color: Colors.grey[900],
                  borderRadius: BorderRadius.circular(4),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: sizeConfig.width(0.0175),
                  vertical: sizeConfig.height(.01),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: sizeConfig.height(.03),
                      child: FittedBox(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                          strokeWidth: 3,
                          value: downloadProgress.progress
                        ),
                      ),
                    ),
                    SizedBox(
                      width: sizeConfig.width(.01),
                    ),
                    Text(
                      "Loading screenshot...",
                      style: Theme.of(context).textTheme.caption.copyWith(
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            errorWidget: (context, url, error) => Icon(Icons.error),
            height: sizeConfig.isDesktopScreen ? sizeConfig.height(0.75): sizeConfig.height(0.85),
            width: _screenImageWidth,
            fit: BoxFit.contain,
            alignment: Alignment.bottomCenter,
          ),
        ),

        ///Gap
        SizedBox(
          height: sizeConfig.height(.015),
        ),

        ///Screen Name
        Text(
          title,
          style: Theme.of(context).textTheme.caption.copyWith(
            fontStyle: FontStyle.italic,
            fontSize: sizeConfig.isMobileScreen ? 22: null,
          ),
        ),

        ///Gap
        SizedBox(
          height: sizeConfig.height(.075),
        ),
      ],
    );
  }
}
