// ignore: avoid_web_libraries_in_flutter
import 'dart:js' as js;
import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:responsive_builder/responsive_builder.dart'
    hide DeviceScreenType;
import 'package:the_food_app/utils/app_theme.dart';
import 'package:the_food_app/utils/globals.dart';
import 'package:the_food_app/widgets/the_food_app_logo.dart';
import 'package:the_food_app/widgets/hover_scale_button.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:the_food_app/widgets/transparent_image.dart';
import 'package:zap_architecture_flutter/zap_architecture_flutter.dart';
import 'package:zap_architecture_flutter/src/helpers.dart';

/// Created by Musa Usman on 27.10.2020
/// Copyright © 2020 Musa Usman. All rights reserved.

part 'landing_page_parts/header_section.dart';

part 'landing_page_parts/features_section.dart';

part 'landing_page_parts/screenshots_section.dart';

part 'landing_page_parts/footer_section.dart';

part 'landing_page_parts/footer_bar.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  void didChangeDependencies() {
    sizeConfig = SizeConfig.init(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: _body(),
        backgroundColor: Colors.black,
      );

  _body() => Container(
        child: SingleChildScrollView(
          physics: ClampingScrollPhysics(),
          padding: EdgeInsets.only(
            bottom: sizeConfig.isDesktopScreen
                ? sizeConfig.height(.0)
                : sizeConfig.height(.0),
          ),
          child: Column(
            children: [
              _HeaderSection(),
              _FeaturesSection(),
              _ScreenshotsSection(),
              _FooterSection(),
              _FooterBar(),
            ],
          ),
        ),
      );
}
